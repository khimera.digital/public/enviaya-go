package models

type WebhookBody struct {
	Status   string         `json:"status,omitempty" example:"success"`
	Action   string         `json:"action,omitempty" example:"shipments.status_update,omitempty" example:"success"`
	Object   string         `json:"object,omitempty" example:"Shipment"`
	ObjectID int            `json:"object_id,omitempty" example:"9999999"`
	Shipment ShipmentStatus `json:"shipment"`
}

type ShipmentStatus struct {
	ShipmentID                  int         `json:"shipment_id,omitempty" example:"9999999"`
	ShipmentNumber              string      `json:"shipment_number,omitempty" example:"U2A3SDJ1"`
	CarrierShipmentNumber       string      `json:"carrier_shipment_number,omitempty" example:"1234567890"`
	Status                      string      `json:"status,omitempty" example:"Recolección pendiente"`
	StatusCode                  int         `json:"status_code,omitempty" example:"13"`
	Event                       string      `json:"event,omitempty" example:"En ruta de entrega"`
	EventCode                   int         `json:"event_code,omitempty" example:"160"`
	EventDescription            string      `json:"event_description,omitempty" example:"En ruta de entrega"`
	SubEvent                    interface{} `json:"sub_event,omitempty"`
	SubEventCode                interface{} `json:"sub_event_code,omitempty"`
	SubEventDescription         interface{} `json:"sub_event_description,omitempty"`
	Label                       string      `json:"label,omitempty" example:"Label base64"`
	NetTotalAmount              string      `json:"net_total_amount,omitempty" example:"100.0"`
	TotalAmount                 string      `json:"total_amount,omitempty" example:"116.0"`
	VatAmount                   string      `json:"vat_amount,omitempty" example:"16.0"`
	OriginFullName              string      `json:"origin_full_name,omitempty" example:"Juan Perez"`
	OriginCompany               string      `json:"origin_company,omitempty" example:"Envia Ya SA de CV"`
	OriginPhone                 string      `json:"origin_phone,omitempty" example:"55 1234 5678"`
	OriginDirection1            string      `json:"origin_direction_1,omitempty" example:"Kepler 195"`
	OriginDirection2            string      `json:"origin_direction_2,omitempty" example:"Int. 2a, Esquina Ejercito Nacional"`
	OriginNeighborhood          string      `json:"origin_neighborhood,omitempty" example:"Nueva Anzures"`
	OriginPostalCode            string      `json:"origin_postal_code,omitempty" example:"11590"`
	OriginCity                  string      `json:"origin_city,omitempty" example:"Ciudad de Mexico"`
	OriginStateCode             string      `json:"origin_state_code,omitempty" example:"DF"`
	OriginCountryCode           string      `json:"origin_country_code,omitempty" example:"MX"`
	OriginEmail                 string      `json:"origin_email,omitempty" example:"juan.perez@enviaya.com.mx"`
	DestinationFullName         string      `json:"destination_full_name,omitempty" example:"Juan Perez"`
	DestinationCompany          string      `json:"destination_company,omitempty" example:"Envia Ya SA de CV"`
	DestinationPhone            string      `json:"destination_phone,omitempty" example:"55 1234 5678"`
	DestinationDirection1       string      `json:"destination_direction_1,omitempty" example:"Kepler 195"`
	DestinationDirection2       string      `json:"destination_direction_2,omitempty" example:"Int. 2a, Esquina Ejercito Nacional"`
	DestinationNeighborhood     string      `json:"destination_neighborhood,omitempty" example:"Nueva Anzures"`
	DestinationPostalCode       string      `json:"destination_postal_code,omitempty" example:"11590"`
	DestinationCity             string      `json:"destination_city,omitempty" example:"Ciudad de Mexico"`
	DestinationStateCode        string      `json:"destination_state_code,omitempty" example:"DF"`
	DestinationCountryCode      string      `json:"destination_country_code,omitempty" example:"MX"`
	DestinationEmail            string      `json:"destination_email,omitempty" example:"juan.perez@enviaya.com.mx"`
	RecalculatedEstDeliveryDate string      `json:"recalculated_est_delivery_date,omitempty" example:"2019-07-26 21:54:27 -0500"`
	EstimatedDeliveryDate       string      `json:"estimated_delivery_date,omitempty" example:"05/08/2019"`
	ExpectedDeliveryDate        string      `json:"expected_delivery_date,omitempty" example:"26/07/2019"`
}
