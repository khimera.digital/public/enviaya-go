package models

type StatesResponse struct {
	States []map[string]string `json:"states"`
}
