package enviaya

import (
	"encoding/json"

	"gitlab.com/khimera.digital/public/enviaya-go/models"
)

const (
	pathTrakings = "/trackings" // POST /api/v1/trackings
)

// ShipmentTracking POST /api/v1/trackings
func (c *client) ShipmentTracking(req models.TrackingRequest) (*models.TrackingResponse, error) {
	// do request
	res, err := c.post(pathTrakings, req)
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.TrackingResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	return r, nil
}
