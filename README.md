# Enviaya Go

A go client library for Enviaya's API

## Examples

### Rate

```go
package main

import (
	"fmt"
	"gitlab.com/khimera.digital/public/enviaya-go"
	enviayamodels "gitlab.com/khimera.digital/public/enviaya-go/models"
)

func main() {
	debug := true
	client := enviaya.New("YOUR_API_KEY", debug)
	res, err := client.Rate(enviayamodels.RateRequest{
		OrderTotalAmount:     100,
		InsuredValue:         100,
		InsuredValueCurrency: "MXN",
		OriginDirection: enviayamodels.Direction{
			PostalCode:  "321654",
			StateCode:   "QR",
			CountryCode: "MX",
		},
		DestinationDirection: enviayamodels.Direction{
			PostalCode:  "654987",
			StateCode:   "QR",
			CountryCode: "MX",
		},
		Shipment: enviayamodels.Shipment{
			ShipmentType: "Package",
			Parcels: []enviayamodels.Parcel{
				{
					Quantity:      1,
					Weight:        1,
					WeightUnit:    "kg",
					Height:        1,
					Length:        1,
					Width:         1,
					DimensionUnit: "cm",
				},
			},
		},
		IntelligentFiltering: true,
	})
	if err != nil {
		panic(err)
	}
	fmt.Sprintf("%+v", *res)
}
```

### Book

```go
package main

import (
	"fmt"
	"gitlab.com/khimera.digital/public/enviaya-go"
	enviayamodels "gitlab.com/khimera.digital/public/enviaya-go/models"
)

func main() {
	debug := true
	client := enviaya.New("YOUR_API_KEY", debug)
	res, err := client.Booking(enviayamodels.ShipmentBookingRequest{
		Carrier:            "dhl",
		CarrierServiceCode: "express",
		RateID:             "123123",
		OriginDirection: enviayamodels.Direction{
			FullName:    "Sponge Bob",
			Direction1:  "Pineapple under the sea",
			Direction2:  "N 123",
			Phone:       "99999999",
			PostalCode:  "6666",
			City:        "Qwerty",
			CountryCode: "MX",
			StateCode:   "QR",
		},
		DestinationDirection: enviayamodels.Direction{
			FullName:    "Patric Star",
			Direction1:  "Under the rock",
			Direction2:  "N 456",
			Phone:       "666666666",
			PostalCode:  "9999",
			City:        "Qwerty",
			CountryCode: "MX",
			StateCode:   "QR",
		},
		Shipment: enviayamodels.Shipment{
			ShipmentType:         "Package",
			Content:              "Gary's bones",
			InsuredValue:         100,
			InsuredValueCurrency: "MXN",
			Parcels: []enviayamodels.Parcel{
				{
					Quantity:      1,
					Weight:        1,
					WeightUnit:    "kg",
					Height:        1,
					Length:        1,
					Width:         1,
					DimensionUnit: "cm",
				},
			},
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Sprintf("%+v", *res)
}
```

### Track

```go
package main

import (
	"fmt"
	"gitlab.com/khimera.digital/public/enviaya-go"
	enviayamodels "gitlab.com/khimera.digital/public/enviaya-go/models"
)

func main() {
	debug := true
	client := enviaya.New("YOUR_API_KEY", debug)
	res, err := client.ShipmentTracking(enviayamodels.TrackingRequest{
		Carrier:        "fedex",
		ShipmentNumber: "321456987",
	})
	if err != nil {
		panic(err)
	}
	fmt.Sprintf("%+v", *res)
}
```
