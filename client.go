package enviaya

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"

	"gitlab.com/khimera.digital/public/enviaya-go/models"
)

// APIURL Base API url
const APIURL = "https://enviaya.com.mx/api/v1"

// Enviaya api
type Enviaya interface {
	GetPostalCodeInformation(countryCode, postalCode string) (*models.PostalCodeInformationResponse, error)
	GetStates(countryCode string) (*models.StatesResponse, error)
	PickupBooking(req models.PickupBookingRequest) (*models.PickupBookingResponse, error)
	Rate(req models.RateRequest) (*models.RateResponse, error)
	Booking(req models.ShipmentBookingRequest) (*models.ShipmentBookingResponse, error)
	LookUp(enviayaID string) (*models.ShipmentLookupResponse, error)
	Cancellation(req models.ShipmentCancellationRequest) (*models.ShipmentCancellationResponse, error)
	ShipmentTracking(req models.TrackingRequest) (*models.TrackingResponse, error)
}

type client struct {
	apiURL string
	apiKey string
	logger logrus.FieldLogger
}

// do get request
func (c *client) get(path string) ([]byte, error) {
	return c.send(http.MethodGet, path, nil)
}

// do post request
func (c *client) post(path string, payload interface{}) ([]byte, error) {
	return c.send(http.MethodPost, path, payload)
}

// do put request
func (c *client) put(path string, payload interface{}) ([]byte, error) {
	return c.send(http.MethodPut, path, payload)
}

// do delete request
func (c *client) delete(path string) ([]byte, error) {
	return c.send(http.MethodDelete, path, nil)
}

func (c *client) send(method, path string, b interface{}) ([]byte, error) {
	// Encode request body
	payload, err := json.Marshal(b)
	if err != nil {
		return nil, err
	}

	// Create http request
	req, err := http.NewRequest(method, fmt.Sprintf("%s%s", c.apiURL, path), bytes.NewReader(payload))
	if err != nil {
		return nil, err
	}

	// Set api key in request
	req.Header.Add("content-type", "application/json")
	req.SetBasicAuth(c.apiKey, "")
	// NOTE: Seems like basic auth isn't working, so we also set the api key as a query param
	q := req.URL.Query()
	q.Add("api_key", c.apiKey)
	req.URL.RawQuery = q.Encode()

	// Do http request
	logger := c.logger.WithFields(map[string]interface{}{
		"method": method,
		"host":   req.URL.Host,
		"path":   req.URL.Path,
	})
	logger.Info("calling remote api")
	if b != nil {
		logger.WithField("payload", string(payload)).Debug("with payload")
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// Read response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logger.WithField("error", err).Info("call failed")
		return nil, err
	}

	logger.WithField("status", res.Status).Info("call completed")
	logger.WithField("response", string(body)).Debug("enviaya response")

	// If status isn't OK (2xx) then try to decode it into a enviaya error
	// If decode fails then return decode (Unmarshal) error and nil response
	if res.StatusCode < 200 || res.StatusCode > 299 {
		logger.WithField("error", string(body)).Info("remote host returned error")
		enviayaErr := &models.Error{}
		if err := json.Unmarshal(body, enviayaErr); err != nil {
			return nil, err
		}
		return nil, enviayaErr
	}
	return body, nil
}

// New creates a new enviaya client instance
// ALERT: be careful, debug level may lead to privacy issues due it will log raw requests/responses
func New(key string, logger logrus.FieldLogger) Enviaya {
	if logger == nil {
		logger = logrus.New()
	}
	return &client{
		apiURL: APIURL,
		apiKey: key,
		logger: logger.WithField("client", "enviaya"),
	}
}
