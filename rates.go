package enviaya

import (
	"encoding/json"

	"gitlab.com/khimera.digital/public/enviaya-go/models"
)

const (
	pathRates = "/rates" // POST https://500.api.enviaya.com.mx/api/v1/rates
)

// Rate POST /api/v1/rates
func (c *client) Rate(req models.RateRequest) (*models.RateResponse, error) {
	// do request
	res, err := c.post(pathRates, req)
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.RateResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	return r, nil
}
