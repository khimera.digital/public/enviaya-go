package enviaya

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/khimera.digital/public/enviaya-go/models"
)

const (
	pathShipments    = "/shipments"            // POST https://500.api.enviaya.com.mx/api/v1/shipments
	pathLookUp       = "/shipments/%s"         // GET https://500.api.enviaya.com.mx/api/v1/shipments/{enviaya_id}?api_key={api_key}
	pathCancellation = "/request_cancellation" // POST https://500.api.enviaya.com.mx/api/v1/request_cancellation
)

// Booking Post https://500.api.enviaya.com.mx/api/v1/shipments
func (c *client) Booking(req models.ShipmentBookingRequest) (*models.ShipmentBookingResponse, error) {
	// do request
	res, err := c.post(pathShipments, req)
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.ShipmentBookingResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	if err := parseEnviayaErrors(r.Errors); err!=nil{
		return nil, err
	}
	return r, nil
}

func parseEnviayaErrors(e interface{}) error {
	err, _ := e.([]interface{})
	if len(err) != 0 {
		return errors.New(fmt.Sprintf("%+v", err))
	}
	return nil
}

// LookUp GET /api/v1/shipments/enviaya_id?api_key=YOUR_API_KEY
func (c *client) LookUp(enviayaID string) (*models.ShipmentLookupResponse, error) {
	// do request
	res, err := c.get(fmt.Sprintf(pathLookUp, enviayaID))
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.ShipmentLookupResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	if err := parseEnviayaErrors(r.Shipment.Errors); err!=nil{
		return nil, err
	}
	return r, nil
}

// Cancellation POST https://500.api.enviaya.com.mx/api/v1/request_cancellation
func (c *client) Cancellation(req models.ShipmentCancellationRequest) (*models.ShipmentCancellationResponse, error) {
	// do request
	res, err := c.post(pathCancellation, req)
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.ShipmentCancellationResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	return r, nil
}
