package enviaya

import (
	"encoding/json"

	"gitlab.com/khimera.digital/public/enviaya-go/models"
)

const (
	pathPickupBooking = "/pickups" // POST /api/v1/pickups
)

// PickupBooking POST /api/v1/pickups
func (c *client) PickupBooking(req models.PickupBookingRequest) (*models.PickupBookingResponse, error) {
	// do request
	res, err := c.post(pathShipments, req)
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.PickupBookingResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	return r, nil
}
