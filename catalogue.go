package enviaya

import (
	"encoding/json"
	"fmt"

	"gitlab.com/khimera.digital/public/enviaya-go/models"
)

const (
	pathPostalCodeInformation = "/get_postal_code_information?country_code=%s&postal_code=%s"
	pathStates                = "/get_states?country_code=%s"
)

// GetPostalCodeInformation GET /get_postal_code_information?country_code=COUNTRY_CODE&page=1&per_page=500&postal_code=POSTAL_CODE"
func (c *client) GetPostalCodeInformation(countryCode, postalCode string) (*models.PostalCodeInformationResponse, error) {
	// do request
	res, err := c.get(fmt.Sprintf(pathPostalCodeInformation, countryCode, postalCode))
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.PostalCodeInformationResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	return r, nil
}

// GetStates GET https://enviaya.com.mx/api/v1/get_states?api_key=YOUR_API_KEY&country_code=mx
func (c *client) GetStates(countryCode string) (*models.StatesResponse, error) {
	// do request
	res, err := c.get(fmt.Sprintf(pathStates, countryCode))
	if err != nil {
		return nil, err
	}
	// decode response
	r := &models.StatesResponse{}
	if err := json.Unmarshal(res, r); err != nil {
		return nil, err
	}
	return r, nil
}
